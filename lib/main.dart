import 'package:flutter/material.dart';
import 'package:circular_countdown_timer/circular_countdown_timer.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'raindrops',
        home: MyHomePage(),
        theme: ThemeData(primaryColor: Colors.white));
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final _controller = CountDownController();
  final _suggestions = <String>[];
  final _saved = Set<String>();

  void _pushedSaved() {
    Navigator.of(context)
        .push(MaterialPageRoute<void>(builder: (BuildContext context) {
      final tiles = _saved.map((String row) {
        return ListTile(title: Text(row));
      });
      final divided =
          ListTile.divideTiles(context: context, tiles: tiles).toList();
      return Scaffold(
          appBar: AppBar(title: Text("Saved suggestions")),
          body: ListView(children: divided));
    }));
  }

  Widget _buildSuggestions() {
    return ListView.builder(
      padding: EdgeInsets.all(16.0),
      itemBuilder: (content, i) {
        if (i.isOdd) return Divider();
        final index = i ~/ 2;
        if (index >= _suggestions.length) {
          _suggestions.add("Interpolation $i");
        }
        return _buildRow(_suggestions[index]);
      },
    );
  }

  Widget _buildRow(String row) {
    final alreadySaved = _saved.contains(row);
    return ListTile(
        title: Text(row),
        trailing: Icon(alreadySaved ? Icons.favorite : Icons.favorite_border,
            color: alreadySaved ? Colors.red : null),
        onTap: () {
          setState(() {
            if (alreadySaved) {
              _saved.remove(row);
            } else {
              _saved.add(row);
            }
          });
        });
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
        appBar: AppBar(
          title: Text("My home page"),
          actions: [
            IconButton(
              icon: Icon(Icons.list),
              onPressed: _pushedSaved,
            )
          ],
        ),
        body: Column(children: [
          Expanded(
              flex: 1,
              child: CircularCountDownTimer(
                duration: 60,
                controller: _controller,
                width: size.width / 2,
                height: size.height / 2,
                color: Colors.grey,
                fillColor: Colors.amber,
              )),
          Expanded(flex: 1, child: Container(child: _buildSuggestions()))
        ]));
  }
}
